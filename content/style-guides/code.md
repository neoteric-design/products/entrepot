+++
title = "Code Style Guide"
description = ""
publishDate = "2020-05-18"
noindex = true
+++
## Inline backticks:

Here's an example of `inline backtick code` and what that looks like. Often for variables or commands.


## Code block with three backticks, toml specified: 

```toml
name = "Contemp"
license = "MIT"
licenselink = "https://gitlab.com/neotericdesign-tools/contemp/blob/master/LICENSE"
description = "A technical blogging theme"
homepage = "http://example.com/"
tags = [""]
features = []
min_version = "0.41"
```

## Code block with backticks

```html
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Example HTML5 Document</title>
</head>
<body>
  <p>Test</p>
</body>
</html>
```

## Code block indented with four spaces

    <!doctype html>
    <html lang="en">
    <head>
      <meta charset="utf-8">
      <title>Example HTML5 Document</title>
    </head>
    <body>
      <p>Test</p>
    </body>
    </html>

## Shortcode Highlight

{{< highlight html >}}
<section id="main">
  <div>
   <h1 id="title">{{ .Title }}</h1>
    {{ range .Pages }}
        {{ .Render "summary"}}
    {{ end }}
  </div>
</section>
{{< /highlight >}}

## Shortcode Highlight: table option

{{< highlight html "linenos=table" >}}
<section id="main">
  <div>
   <h1 id="title">{{ .Title }}</h1>
    {{ range .Pages }}
        {{ .Render "summary"}}
    {{ end }}
  </div>
</section>
{{< /highlight >}}

## Go HTML template: Linenos True

{{< highlight go-html-template "linenos=true" >}}
<nav id="primary-nav" class="row" role="navigation">
  {{ if .Site.Menus.main }}
    <ul class="menu">
      {{ range .Site.Menus.main }}
        <li class=""><a class="" href="{{ .URL }}" title="{{ .Name }} page">{{ .Name }}</a></li>
      {{ end }}
    </ul>
    <hr />
  {{- end -}}
</nav>
{{< /highlight >}}

## Go HTML template: Linenos False

{{< highlight go-html-template "linenos=false" >}}
<nav id="primary-nav" class="row" role="navigation">
  {{ if .Site.Menus.main }}
    <ul class="menu">
      {{ range .Site.Menus.main }}
        <li class=""><a class="" href="{{ .URL }}" title="{{ .Name }} page">{{ .Name }}</a></li>
      {{ end }}
    </ul>
    <hr />
  {{- end -}}
</nav>
{{< /highlight >}}

## Go HTML template: Linenos Inline

{{< highlight go-html-template "linenos=inline" >}}
<nav id="primary-nav" class="row" role="navigation">
  {{ if .Site.Menus.main }}
    <ul class="menu">
      {{ range .Site.Menus.main }}
        <li class=""><a class="" href="{{ .URL }}" title="{{ .Name }} page">{{ .Name }}</a></li>
      {{ end }}
    </ul>
    <hr />
  {{- end -}}
</nav>
{{< /highlight >}}